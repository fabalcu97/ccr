/* Server code in C */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <thread>

using namespace std;

void connectClient(int ConnectFD) {

  char buffer[256];
  int n;
  
  do
  {
    buffer[0] = 0;
    n = read(ConnectFD, buffer, 255);
    strcat(buffer, " -- Received");
    n = write(ConnectFD, buffer, strlen(buffer));
  } while (*buffer != '#');
  
  close(ConnectFD);
}

int main(void)
{
  struct sockaddr_in stSockAddr;
  struct sockaddr_in cli_addr;
  int SocketFD;
  socklen_t size;
  int n;

  if ((SocketFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("Socket");
    exit(1);
  }

  if (setsockopt(SocketFD, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(int)) == -1)
  {
    perror("Setsockopt");
    exit(1);
  }

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(3000);
  stSockAddr.sin_addr.s_addr = INADDR_ANY;

  if (bind(SocketFD, (struct sockaddr *)&stSockAddr, sizeof(struct sockaddr)) == -1)
  {
    perror("Unable to bind");
    exit(1);
  }

  if (listen(SocketFD, 5) == -1)
  {
    perror("Listen");
    exit(1);
  }

  size = sizeof(stSockAddr);

  while(1)
  {
    int ConnectFD = accept(SocketFD, (struct sockaddr *)&cli_addr, &size);
    cout<<ConnectFD<<endl;
    thread(connectClient, ConnectFD).detach();
  }

  close(SocketFD);
  return 0;
}
