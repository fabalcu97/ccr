/* Client code in C */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

using namespace std;

int main(int argc, char *argv[])
{
  struct sockaddr_in stSockAddr;
  int Res;
  int SocketFD = socket(AF_INET, SOCK_STREAM, 0); //IPPROTO_TCP
  int n;
  char buffer[256];
  char buffer2[256];

  if (-1 == SocketFD)
  {
    cout<<"cannot create socket"<<endl;
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(atoi(argv[2]));
  Res = inet_pton(AF_INET, argv[1], &stSockAddr.sin_addr);

  if (0 > Res)
  {
    cout<<"error: first parameter is not a valid address family"<<endl;
    close(SocketFD);
    exit(EXIT_FAILURE);
  }
  else if (0 == Res)
  {
    cout<<"char string (second parameter does not contain valid ipaddress"<<endl;
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == connect(SocketFD, (const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
  {
    cout<<"connect failed"<<endl;
    close(SocketFD);
    exit(EXIT_FAILURE);
  }
  do {
    n = recv(SocketFD, buffer, 255, 0);
    cout << "Server says: " << buffer << endl;

    cout<<"Send text:";
    cin>>buffer2;
    n = send(SocketFD, buffer2, strlen(buffer2), 0);
  } while(*buffer != '#');
  

  shutdown(SocketFD, SHUT_RDWR);

  close(SocketFD);
  return 0;
}
