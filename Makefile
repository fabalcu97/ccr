GCC = gcc
G++ = g++

LIBS =

sockets-secuential:
	@ $(G++) $(LIBS) Secuential/server.cpp -o Secuential/bin/server.out
	@ $(G++) $(LIBS) Secuential/client.cpp -o Secuential/bin/client.out

sockets-parallel:
	@ $(G++) $(LIBS) Parallel/server.cpp -o bin/server.out
	@ $(G++) $(LIBS) Parallel/client.cpp -o bin/client.out

clean:
	@ rm *.out